import pathlib

from flair.data import Sentence
from flair.models import RelationExtractor, SequenceTagger


# For Windows OS only!
temp = pathlib.PosixPath
pathlib.PosixPath = pathlib.WindowsPath

# 1. make example sentence
sentence = Sentence("Wie Elser im Berliner Verhörprotokoll erklärte, trat er 1928 oder 1929 in Konstanz dem Roten Frontkämpferbund bei, dem paramilitärischen Kampfverband der KPD.")

# 2. load entity tagger and predict entities
tagger = SequenceTagger.load('../models/ner-bert-finetuned/final-model.pt')
tagger.predict(sentence)

# check which entities have been found in the sentence
entities = sentence.get_labels('ner')
for entity in entities:
    print(entity)

# 3. load relation extractor
extractor: RelationExtractor = RelationExtractor.load('../models/relation-extraction/final-model.pt')

# predict relations
extractor.predict(sentence)

# check which relations have been found
relations = sentence.get_labels('relation')
for relation in relations:
    print(relation)
