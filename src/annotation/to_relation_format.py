import json


def build_relations_file():
    """
    converts the initial relation classes into a format processable for doccano
    """
    lines = []
    with open("annotation_classes.txt", 'r', encoding='UTF-8') as f:
        lines = f.readlines()
    relation_classes = [
        {'text': line.strip(),
         'background_color': '#FF0000',
         'text_color': '#ffffff'
         }
        for line in lines if ':' not in line and line.strip()]
    with open('annotation_classes.json', 'w', encoding='UTF-8') as f:
        json.dump(relation_classes, f, ensure_ascii=False)







build_relations_file()
