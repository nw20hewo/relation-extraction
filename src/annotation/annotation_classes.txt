PER_LOC: #FF0000
    geboren_in
    wohnt_in
    arbeitet_in
    stationiert_in
    Aufenthalt_in
    Ausbildung_in
    inhaftiert_in
    gestorben_in
    ermordet_in

PER_ORG: #0062B1
    Mitglied_bei
    Führungsposition_in
    arbeitet_bei
    Kontakt_zu
    verfolgt_von
    eingezogen_zu

ORG_LOC: #653294
    Gründungsort
    aktiv_in

PER_PER: #194D33
    Eltern/Kind
    Geschwister
    Freundschaft
    Arbeitskollegen
    Bekanntschaft
    ermordet_von
    Gegner_von
    Ehe_mit
    Beziehung_mit

LOC_LOC: #FCDC00
    liegt_in
    liegt_bei

ORG_ORG: #0C797D
    kollaboriert_mit
    Opposition_zu