"""Data preprocessing the data for the model."""

import argparse
import json


class Entity:
    def __init__(self, data, text):
        self.id = data["id"]
        self.label = data["label"]
        self.start_offset = data["start_offset"]
        self.end_offset = data["end_offset"]
        self.words = text[self.start_offset : self.end_offset].split(" ")
        # +1 since python is index 0 based but conll is 1 based
        self.sentence_ids = [
            text.split(" ").index(word) + 1
            for word in self.words
            if word in text.split(" ")
        ]

    def get_label(self):
        """Turn entities into BIO-Format."""
        labels = []
        for index, _ in enumerate(self.words):
            if index == 0:
                labels.append("B-" + self.label)
            if index > 0:
                labels.append("I-" + self.label)
        return labels

    def get_sentence_ids(self, text):
        """Get the position id of entity in sentence."""
        index = 0
        text_dict = {}  # key: index, value: (word, start_offset, end_offset)
        for i, word in enumerate(text.split(" ")):
            start_offset = index
            end_offset = index + len(word)
            index += len(word) + 1
            text_dict[i + 1] = (word, start_offset, end_offset)
        sentence_ids = []
        for key, word_tuple in text_dict.items():
            start_offset = word_tuple[1]
            end_offset = word_tuple[2]
            if start_offset in range(self.start_offset, self.end_offset):
                sentence_ids.append(key)
        return sentence_ids


class Relation:
    def __init__(self, data, entities):
        self.id = data["id"]
        self.from_id = data["from_id"]
        self.to_id = data["to_id"]
        self.type = data["type"]
        self.entities = entities

    def get_entities(self) -> tuple[Entity, Entity]:
        """Get the entities belonging to a relation."""
        # return tuple of Entity objects from the entities list where the first matches the from_id and the second matches the to_id
        for entity in self.entities:
            if entity.id == self.from_id:
                from_entity = entity
            if entity.id == self.to_id:
                to_entity = entity
        return from_entity, to_entity


class DoccanoAnnotation:
    def __init__(self, data):
        self.data = data
        self.id = data["id"]
        self.text = data["text"]
        self.name = data["name"]
        self.entities = [Entity(entity, self.text) for entity in data["entities"]]
        self.relations = [
            Relation(relation, self.entities) for relation in data["relations"]
        ]

    def get_relations_conll(self) -> str:
        """Retrieves for each Doccano Annotation belonging relations ConLL file."""
        relations_str = ""
        if not self.relations:
            # handling empty relations as stated in github issue: https://github.com/flairNLP/flair/issues/2726#issuecomment-1205284621
            return relations_str
        else:
            relations_str += " "
            for relation in self.relations:
                entities = relation.get_entities()
                first_entity = f"{entities[0].get_sentence_ids(self.text)[0]};{entities[0].get_sentence_ids(self.text)[-1]}"
                second_entity = f"{entities[1].get_sentence_ids(self.text)[0]};{entities[1].get_sentence_ids(self.text)[-1]}"
                if relations_str != " ":
                    relations_str += "|"
                relations_str += f"{first_entity};{second_entity};{relation.type}"
        return relations_str

    def get_conll_format(self) -> str:
        """Returns ConLL format for DoccanoAnnotation

        Returns:
            str: ConLL Format string
        """

        conll_format = f"# text = {self.text}\n"
        conll_format += f"# relations ={self.get_relations_conll()}\n"
        entity_words = [item for entity in self.entities for item in entity.words]
        for i, word in enumerate(self.text.split(" ")):
            if word in entity_words:
                for entity in self.entities:
                    # position of the word in the entity
                    if word in entity.words:
                        position = entity.words.index(word)
                        conll_format += (
                            f"{i+1}\t {word}\t {entity.get_label()[position]}\n"
                        )
                        break
            else:
                conll_format += f"{i+1}\t {word}\t O\n"
        return conll_format


class DoccanoData:
    def __init__(self, data_path):
        with open(data_path, "r") as f:
            self.data = [json.loads(line) for line in f]

    def get_annotations(self) -> list[DoccanoAnnotation]:
        """Retrieves list of doccano annotations"""
        return [DoccanoAnnotation(annotation) for annotation in self.data]

    def get_conll_format(self) -> list[str]:
         """Retrieves ConLL format for the list of annotations"""
        conll_annotations = []
        for annotation in self.get_annotations():
            conll_annotations.append(annotation.get_conll_format())
        return conll_annotations


def _parse_args():
    parser = argparse.ArgumentParser(description="Convert Doccano data to CoNLL format")
    parser.add_argument(
        "--data_path",
        type=str,
        help="Path to the data file in json format",
        required=True,
    )

    parser.add_argument(
        "--output_path",
        type=str,
        help="path to the output directory",
        required=True,
    )

    return parser.parse_args()


if __name__ == "__main__":

    args = _parse_args()
    data_path = args.data_path
    output_path = args.output_path
    doccano_data = DoccanoData(data_path)

    person_data: dict = {}

    for sent in doccano_data.data:
        name = sent["name"]
        person_data[name] = []

    output_sents = []
    for name in person_data.keys():
        person_sents = []
        for sent in doccano_data.data:
            if name == sent["name"]:
                person_sents.append(sent)
        output_sents.append(person_sents)

    size_train = int(len(output_sents) * 0.8)
    size_val = int(len(output_sents) * 0.1)
    size_test = int(len(output_sents) * 0.1)

    train = output_sents[:size_train]
    val = output_sents[size_train : size_train + size_val]
    test = output_sents[size_train + size_val :]

    output_data = {
        "train": train,
        "val": val,
        "test": test,
    }

    for key, data in output_data.items():
        with open(f"{output_path}/{key}.conll", "w") as f:
            for person in data:
                for sent in person:
                    f.write(
                        DoccanoAnnotation(sent).get_conll_format() + "\n",
                    )
