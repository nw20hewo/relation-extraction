## Run this file with the_connectionists/ as Working Dir!
import json

import i_o
import glob


class Evaluate:
    def __init__(self):
        self.ground_truth = []
        val_file_names = glob.glob('data/test/*.jsonl')
        for val_file in val_file_names:
            with open(val_file, 'r', encoding='UTF-8') as f:
                lines = f.readlines()
                for line in lines:
                    self.ground_truth.append(json.loads(line))

        self.confusion_matrix = {'true_positive': 0,
                                 'false_positive': 0,
                                 'true_negative': 0,
                                 'false_negative': 0
                                 }

    def evaluate_ba(self):
        """
        evaluate the results of the basline method and write them in self.confusion_matrix
        This can be seen as a best case scenario as it is assumed that every found relation has a correct relation class and the correct named entites.
        :return:
        """
        ba_results = i_o.read_pkl('validation_connectionists_no_fine_tune_2022-08-24')
        relations = ba_results[0]
        for sent in self.ground_truth:


            pop = False
            cont = False
            per = relations[sent['name']]

            for p in per:

                if (
                        (sent['text'] in p.source
                         or p.source in sent['text'])
                        and len(sent['relations']) == 1
                ):
                    self.confusion_matrix['true_positive'] += 1
                    sent['relations'].pop(0)
                    cont = True
                    pop = True
                    break
                elif (
                        (sent['text'] in p.source
                         or p.source in sent['text'])
                        and len(sent['relations']) > 1
                ):
                    self.confusion_matrix['true_positive'] += 1
                    pop = True
                    sent['relations'].pop(0)
                elif (
                        (sent['text'] in p.source
                         or p.source in sent['text'])
                        and len(sent['relations']) == 0
                ):
                    self.confusion_matrix['false_positive'] += 1
                    pop = True
                    cont = True
                    break
                elif (
                        (sent['text'] not in p.source
                         or p.source not in sent['text'])
                        and len(sent['relations']) != 0
                ):
                    self.confusion_matrix['false_negative'] += 1
                    sent['relations'].pop(0)
                    cont = True
                    break
                elif (
                        (sent['text'] not in p.source
                         or p.source not in sent['text'])
                        and len(sent['relations']) == 0
                ):
                    self.confusion_matrix['true_negative'] += 1
                    cont = True
                    break

            if pop:
                per.pop(0)
            if cont:
                self.confusion_matrix['false_negative'] += len(sent['relations'])
                continue
            if len(per) == 0 and len(sent['relations']) > 0:
                self.confusion_matrix['false_negative'] += len(sent['relations'])
            elif len(per) == 0 and len(sent['relations']) == 0:
                self.confusion_matrix['true_negative'] += 1



        self.confusion_matrix

    def calc_confusion_values(self):
        true_positive = self.confusion_matrix['true_positive']
        false_positive = self.confusion_matrix['false_positive']
        true_negative = self.confusion_matrix['true_negative']
        false_negative = self.confusion_matrix['false_negative']

        recall = true_positive/(true_positive+false_negative)
        precision = true_positive/(true_positive+false_positive)

        test_f1 = 2*recall*precision/(recall+precision)
        f1_score = 2*true_positive/(2*true_positive + false_positive + false_negative)

        result = f"""
    PP  PN
P\t{true_positive}\t{false_negative}
N\t{false_positive}\t{true_negative}

recall: {recall}
precision: {precision}
f1 score: {f1_score}"""

        print(result)
        with open("evaluation/evaluation_ba_no_finetune.txt", 'w') as f:
            f.write(result)


evaluate = Evaluate()
evaluate.evaluate_ba()
evaluate.calc_confusion_values()
