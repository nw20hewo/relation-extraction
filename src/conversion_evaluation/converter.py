## Run this file with the_connectionists/ as Working Dir!
import glob

from flair.tokenization import SegtokSentenceSplitter

from src.conversion_evaluation import i_o

from flair.models import SequenceTagger
from flair.data import Sentence


class Trainer:
    def __init__(self,file):
        self.all_text = i_o.read_json(file)

    def build_dataset(self):
        """
        Function to load possibly needed model for tagging.

        :param f: the file
        :return:
        """
        self.ner_model = "de-ner-large"
        #self.ner_tagger = MultiTagger.load([self.ner_model, "de-pos"])
        self.ner_tagger = SequenceTagger.load(self.ner_model)
        self.splitter = SegtokSentenceSplitter()

        # leave following  lines for testing only
        #all_text = i_o.read_json(file)
        #at = []
        #at.append(all_text[1])
        #self.all_text = at

        relations = []
        self.to_json_relation()
        #self.to_conll()


    def to_json_relation(self):
        """
        Converts the specified dataset to a format processable by doccano relation annotation
        """
        processed_pers = [x[13:-6] for x in glob.glob("data/persons/**.jsonl")]
        processed_pers.extend([x[20:-6] for x in glob.glob("data/persons/upload/*.jsonl")])

        for pers in self.all_text:
            name = list(pers.keys())[0]
            if name not in processed_pers:
                pers_text = pers[name]

                sent_list = []

                tagged_sents = self.splitter.split(pers_text)
                self.ner_tagger.predict(tagged_sents)

                for sent in tagged_sents:
                    try:
                        ne_list = sent.annotation_layers['ner']
                    except KeyError:
                        try:
                            ne_list = sent.annotation_layers[self.ner_model]
                        except KeyError:
                            ne_list = []
                    if sent.tokenized:
                        sent_dict = {'name': name, "text": sent.tokenized, 'label': [], 'relations': []}
                        for i in range(len(ne_list)):
                            next_ne = ne_list[i]
                            entity_list = [ next_ne.data_point.start_position,
                                            next_ne.data_point.end_position,
                                            next_ne.data_point.tag]
                            if next_ne.data_point.text != sent.text[next_ne.data_point.start_position:next_ne.data_point.end_position]:
                                start_point_in_tokenized = sent.text.find(next_ne.data_point.text, next_ne.data_point.start_position)
                                end_point_in_tokenized = start_point_in_tokenized + len(next_ne.data_point.text)
                                t = sent.text[start_point_in_tokenized:end_point_in_tokenized]

                                entity_list = [start_point_in_tokenized,
                                               end_point_in_tokenized,
                                               next_ne.data_point.tag]

                            sent_dict['label'].append(entity_list)

                        sent_list.append(sent_dict)

                i_o.write_json_lines(sent_list, f'persons/{name}')




    def to_conll(self):
        """
        Converts the specified data into CoNLL format.
        :return:
        """
        with open("data/biographies.txt", 'w', encoding="UTF-8") as f:
            #f.write(f'#global.columns = id form pos ner\n')
            for pers in self.all_text:
                name = list(pers.keys())[0]
                pers_text = pers[name]

                #Write the persons name/wikipedia title on top, that will help splitting by text.
                #f.write(f"##{name}\n")
                #Split sentences in text and tag NE and POS
                tagged_sents = self.splitter.split(pers_text)
                self.ner_tagger.predict(tagged_sents)
                for sent in tagged_sents:
                    #generate sentence id
                    #sent_id = uuid.uuid4()
                    #f.write(f'\n#text = {sent.tokenized}\n')
                    #f.write(f'#sentence_id = {sent_id}\n')
                    #f.write(f'#relations =\n')

                    try:
                        ne_list = sent.annotation_layers[self.ner_model]
                        next_ne = ne_list.pop(0)
                    except KeyError:
                        ne_list = []
                    for tkn in sent.tokens:
                        f.write(f'{tkn.form}\t')
                        if len(ne_list) == 0 and not next_ne:
                            f.write('O\n')
                        elif tkn.start_pos == next_ne.data_point.start_position and tkn.end_pos == next_ne.data_point.end_position:
                            f.write(f'{next_ne.value}\n')
                            try:
                                next_ne = ne_list.pop(0)
                            except IndexError:
                                next_ne = None
                        elif tkn.start_pos == next_ne.data_point.start_position:
                            f.write(f'B-{next_ne.value}\n')
                        elif tkn.start_pos > next_ne.data_point.start_position and tkn.end_pos < next_ne.data_point.end_position:
                            f.write(f'I-{next_ne.value}\n')
                        elif tkn.end_pos == next_ne.data_point.end_position:
                            f.write(f'E-{next_ne.value}\n')
                            try:
                                next_ne = ne_list.pop(0)
                            except IndexError:
                                next_ne = None
                        else:
                            f.write('O\n')
                    f.write('\n')


    def jsonl_to_ba_json(self):
        """
        Converts from doccano output format to a format suitable for the ba_pipeline
        """

        pers_to_convert = [x[10:-6] for x in glob.glob("data/test/*.jsonl")]
        output = []
        for pers in self.all_text:
            name = list(pers.keys())[0]
            if name in pers_to_convert:
                per_ba_format = {"Name": name,
                                 "Lebensdaten": "",
                                 "Beschreibung": pers[name],
                                 "Literatur": "wikipedia.org"}
                output.append(per_ba_format)
        i_o.write_json(output, "val_set_ba_format")

#Trainer('resistance').build_dataset()
Trainer('resistance').jsonl_to_ba_json()
