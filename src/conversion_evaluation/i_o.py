import json
import pickle
import logging
from datetime import date
"""
The functions in this file can be used to print output in different file formats.
"""
logger = logging.getLogger("dndw")


def read_json(file_to_load) -> dict:
    logger.info(f"loading {file_to_load}.json")
    with open(f"data/{file_to_load}.json", "r", encoding="UTF-8") as read_file:
        return json.load(read_file)


def write_pkl(collection, filename):
    with open(f'data/{filename}.pkl', 'wb') as file:
        pickle.dump(collection, file)
        file.close()
    logger.info(f"data saved in {filename}.pkl")


def read_pkl(filename):
    logger.info(f"loading {filename}.pkl")
    try:
        with open(f'data/{filename}.pkl', 'rb') as file:
            relation_collection = pickle.load(file)
            return relation_collection
    except FileNotFoundError:
        logger.error(f"No file {filename}.pkl found.")
        return {}


def write_json(data,filename):
    """Speichere Ergebnisse"""

    with open(f"data/{filename}.json", "w", encoding="UTF-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    logger.info(f"data saved in {filename}.json")

def write_json_lines(data,filename):
    """Speichere Ergebnisse"""

    with open(f"data/{filename}.jsonl", "w", encoding="UTF-8") as f:
        for d in data:
            json.dump(d, f, ensure_ascii=False)
            f.write('\n')
    logger.info(f"data saved in {filename}.json")


def write_repl_valid(sent, name, filename):
    with open(f"data/{filename}_{date.today()}.txt", "a+", encoding="UTF-8") as f:
        f.write(name + '\t' + sent + '\n')
    logger.info(f"data saved in {filename}.txt")


