import re
from typing import Union


class Entity:
    """
    This class was part of the bachelor thesis and is used to evaluate the results introduced in 4.2 in the paper.
    """
    def __init__(self, name: str):
        #self.id = uuid.uuid4()
        self.name = name
        self.shorts = {name}
        self.shorts.update(name.split())
        self.shorts.update(name.split('_'))
        self.sources = [str]
        self.wikificated = False

    def add_shorts(self, shorts: str):
        self.shorts.update(shorts.split())
        self.shorts.update(shorts.split('_'))
        self.shorts.add(shorts)

    def set_sources(self, sources: []):
        self.sources.extend(sources)

    def add_source(self, source:str):
        self.sources.append(source)

    def set_wikificated(self):
        self.wikificated = True

class Person(Entity):
    def __init__(self, name, lebensdaten=None):
        if lebensdaten:
            self.birthday = Date(lebensdaten.split('-')[0])
            self.day_of_death = Date(lebensdaten.split('-')[1])
        Entity.__init__(self, name)

    def set_birthday(self, birthday):
        self.birthday = birthday

    def set_day_of_death(self, day_of_death):
        self.day_of_death = day_of_death


class Organisation(Entity):
    def __init__(self, name):
        Entity.__init__(self, name)


class Location(Entity):
    def __init__(self, name):
        Entity.__init__(self, name)
        self.coordinates = ""


class Relation:
    def __init__(self, ent_1:Entity, ent_2:Entity, rel_type:list[str], source:str, date=None):
        self.ent1 = ent_1
        self.ent2 = ent_2
        self.type = rel_type
        self.source = source
        self.is_event = False
        if date == "Ohne Datum":
            self.date = [Date(date)]
        elif date:
            self.date = [Date(dt) for dt in date]
            self.is_event = True


class Date:
    def __init__(self, date:Union[list[str], str]):
        self.year = ""
        self.month = ""
        self.day = ""
        if date == "Ohne Datum" or date == "":
            self.org_date = [date]
        else:
            self.date_pattern(date)
            self.org_date = date

    def date_pattern(self, date):
        """Sucht verschiedene Datums-Pattern"""
        # 1. Juli 1920; 1. Juli
        DATE_PATTERN_1 = re.compile(
            r'\d{1,2}\.?\s*(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\w*(?:\s*\d{4}|\d{2})?',
            re.IGNORECASE)
        # 1920; 1920/21
        DATE_PATTERN_2 = re.compile(r'1\d{3}(?:/1\d{1,3}|/\d{2})?')
        # Juli 1920; Juli 20
        DATE_PATTERN_3 = re.compile(
            r'(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\s+(?:1\d{3}|\d{2})(?:/1\d{1,3}|/\d{2})?',
            re.IGNORECASE)

        # gefundene Datumsangaben mit Tag versehen
        if re.match(DATE_PATTERN_1, date):
             date = re.split(r'\W+', date)
             self.day = date[0]
             self.month = date[1]
             try:
                self.year = date[2]
             except IndexError:
                 self.year = ""
        elif re.match(DATE_PATTERN_3, date):
            date = date.split()
            self.month = date[0]
            self.year = date[1]
        elif re.match(DATE_PATTERN_2, date):
            self.year = date



