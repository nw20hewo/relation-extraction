"""Train the Named Entity Recognition (NER) model"""

import argparse

import numpy as np
from flair.data import Corpus, Sentence
from flair.datasets import CONLL_03, ColumnCorpus
from flair.embeddings import TransformerWordEmbeddings
from flair.models import RelationExtractor, SequenceTagger
from flair.trainers import ModelTrainer

import train


def train(
    corpus: ColumnCorpus,
    sequence_tagger: str = "flair/ner-german-large",
    learning_rate: float = 0.01,
    mini_batch_size: int = 32,
    max_epochs: int = 10,
    output_path: str = "/models/ner",
):
    # 1. load flair ner sequencetagger model
    tagger = SequenceTagger.load(sequence_tagger)
    print("tagger", sequence_tagger)
    # 3. create model trainer
    # train the modeltrainer with ner-german model by flair and our corpus
    trainer: ModelTrainer = ModelTrainer(tagger, corpus)
    print("output_path", output_path)
    # 4. start training with low learning_rate based on this github issue: https://github.com/flairNLP/flair/issues/1540
    trainer.train(
        output_path=output_path,
        learning_rate=learning_rate,
        mini_batch_size=mini_batch_size,
        max_epochs=max_epochs,
    )


def finetune_bert(
    corpus: ColumnCorpus,
    learning_rate: float = 0.01,
    mini_batch_size: int = 32,
    max_epochs: int = 10,
    output_path: str = "/models/ner-bert-embeddings",
):
    print(output_path)

    # 1. what label do we want to predict?
    label_type = "ner"

    # 2. make the label dictionary from the corpus
    label_dict = corpus.make_label_dictionary(label_type=label_type)

    # 3. initialize fine-tuneable transformer embeddings WITH document context
    embeddings = TransformerWordEmbeddings(
        model="bert-base-german-cased",
        layers="-1",
        subtoken_pooling="first",
        fine_tune=True,
        use_context=True,
    )

    # 4. initialize bare-bones sequence tagger (no CRF, no RNN, no reprojection)
    tagger = SequenceTagger(
        hidden_size=256,
        embeddings=embeddings,
        tag_dictionary=label_dict,
        tag_type="ner",
        use_crf=False,
        use_rnn=False,
        reproject_embeddings=False,
    )

    # 5. initialize trainer
    trainer = ModelTrainer(tagger, corpus)

    # 7. run training
    # trainer.train('/content/drive/MyDrive/bdlt/new/bert_epochs_150/taggers/sota-ner-flert',
    #              train_with_dev=True,
    #              mini_batch_size=16,
    #              max_epochs=10,
    #              main_evaluation_metric=("macro avg", "f1-score"))

    # 6. run fine-tuning
    trainer.fine_tune(
        base_path=output_path,
        learning_rate=5.0e-6,
        mini_batch_size=mini_batch_size,
        max_epochs=max_epochs
    )


def _parse_args():
    parser = argparse.ArgumentParser(description="Train a ner model on a corpus")
    parser.add_argument(
        "--data_path",
        type=str,
        help="Path to the data directory containing the data files in conll format, named: {train, val, test}.conll",
        required=True,
    )

    parser.add_argument(
        "--output_path",
        type=str,
        help="path to the output directory",
        # required=True,
        default="models/ner",
    )

    parser.add_argument(
        "--max_epochs",
        type=int,
        help="max epochs",
        default=250,
    )

    parser.add_argument(
        "--batch_size",
        type=int,
        help="mini batch size",
        default=32,
    )

    parser.add_argument(
        "--learning_rate",
        type=float,
        help="learning rate",
        default=0.01,
    )

    return parser.parse_args()


if __name__ == "__main__":

    args = _parse_args()
    data_folder = args.data_path

    output_path = args.output_path
    max_epochs = args.max_epochs
    batch_size = args.batch_size
    learning_rate = args.learning_rate

    file_name_train = "train.conll"
    file_name_dev = "val.conll"
    file_name_test = "test.conll"

    # create our corpus based on conll format data
    columns = {1: "text", 2: "ner"}

    # initializing the corpus
    corpus: Corpus = ColumnCorpus(
        data_folder,
        columns,
        train_file=file_name_train,
        dev_file=file_name_dev,
        test_file=file_name_test,
    )

    finetune_bert(
        corpus=corpus,
        learning_rate=learning_rate,
        mini_batch_size=batch_size,
        max_epochs=max_epochs,
        output_path=output_path,
    )
    # sequence_tagger = "flair/ner-german"
    # train(
    #     corpus=corpus,
    #     sequence_tagger=sequence_tagger,
    #     learning_rate=learning_rate,
    #     mini_batch_size=batch_size,
    #     max_epochs=max_epochs,
    #     output_path=output_path,
    # )
