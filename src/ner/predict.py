"""Predicts Named Entities with trained model and saves output to evaluation dir."""

import torch
import flair
import json
from flair.data import Sentence, Corpus
from flair.models import SequenceTagger
from flair.datasets import ColumnCorpus
from pathlib import Path

model: SequenceTagger = SequenceTagger.load('models/ner/ner-500/final-model.pt')
#model: SequenceTagger = SequenceTagger.load('flair/ner-german-large')

columns = {1: 'text', 2: 'ner'}

data_folder = "data"

# initializing the corpus
corpus: Corpus = ColumnCorpus(data_folder, columns,
                              train_file="biographical_wiki_data.conll")

result = model.evaluate(corpus.train + corpus.dev + corpus.test, 'ner')

with open('./evaluation/ner-500-predict_biographical_data.log', 'w') as f:
    f = f.write(result.detailed_/mnt/ceph/storage/data-tmp/teaching-current/nw20hewo/relation-extraction/src/relation_extractionresults)