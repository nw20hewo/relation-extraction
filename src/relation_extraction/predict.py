"""Predicts relations with trained model and saves output to evaluation dir."""
import torch
import flair
import json
from flair.data import Sentence, Corpus
from flair.models import RelationExtractor
from flair.datasets import ColumnCorpus
from pathlib import Path

model: RelationExtractor = RelationExtractor.load('models/relation_extraction/re-100/final-model.pt')

columns = {1: 'text', 2: 'ner'}

data_folder = "data"

# initializing the corpus
corpus: Corpus = ColumnCorpus(data_folder, columns,
                              train_file="biographical_wiki_data.conll")

result = model.evaluate(corpus.train + corpus.dev + corpus.test, 'relation')

with open('./evaluation/re-100-predict_bio.txt', 'w') as f:
    f = f.write(result.detailed_results)