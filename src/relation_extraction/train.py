"""Train the relation extraction model."""
import argparse

import numpy as np
from flair.data import Corpus, Sentence
from flair.datasets import CONLL_03, ColumnCorpus
from flair.embeddings import TransformerWordEmbeddings
from flair.models import RelationExtractor, SequenceTagger
from flair.trainers import ModelTrainer
from torch.optim.adam import Adam


#flair.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def train(
    corpus: ColumnCorpus,
    sequence_tagger: str = "flair/ner-german-large",
    learning_rate: float = 1e-5,
    mini_batch_size: int = 64,
    max_epochs: int = 10,
    output_path: str = "./models/relation_extraction_1_epochs",
    transformer:str = "bert-base-german-cased"
):  
    print(
        f'''
        sequence_tagger: {sequence_tagger}
        learning_rate: {learning_rate}
        mini_batch_size: {mini_batch_size}
        max_epochs: {max_epochs}
        output_path: {output_path}
        transformer: {transformer}'''
    )

    # Step 2: Make the label dictionary from the corpus
    label_dictionary = corpus.make_label_dictionary("relation")
    label_dictionary.add_item("O")

    # Step 3: Initialize fine-tunable transformer embeddings
    embeddings = TransformerWordEmbeddings(model=transformer, layers="-1", fine_tune=True)

    # Step 4: Initialize relation classifier
    model: RelationExtractor = RelationExtractor(
        embeddings=embeddings,
        label_dictionary=label_dictionary,
        label_type="relation",
        entity_label_type="ner",
        # entity_pair_filters= [
        #    ('PER', 'PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, neuer_Name, verfolgt_von
        #    ('Coref_PER', 'PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, neuer_Name, verfolgt_von
        #    ('PER', 'Coref_PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, verfolgt_von
        #    ('Coref_PER', 'Coref_PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, verfolgt_von
        #    ('PER', 'LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('Coref_PER', 'LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('PER', 'Coref_LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('Coref_PER', 'Coref_LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('PER', 'ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('Coref_PER', 'ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('PER', 'Coref_ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('Coref_PER', 'Coref_ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('ORG', 'LOC'), # liegt_in
        #    ('Coref_ORG', 'LOC'), # liegt_in
        #    ('ORG', 'Coref_LOC'), # liegt_in
        #    ('Coref_ORG', 'Coref_LOC'), # liegt_in,
        #    ('ORG', 'ORG'), # verfolgt_von
        #    ('ORG', 'Coref_ORG'), # verfolgt_von
        #    ('Coref_ORG', 'ORG'), # verfolgt_von
        #    ('Coref_ORG', 'Coref_ORG'), # verfolgt_von
        #   ]
    )

    # Step 5: Initialize trainer
    trainer: ModelTrainer = ModelTrainer(model, corpus)

    # Step 6: Run fine-tuning
    trainer.fine_tune(
        base_path=output_path,
        max_epochs=max_epochs,
        learning_rate=learning_rate,
        mini_batch_size=mini_batch_size,
        )

def find_learning_rate(
    corpus: ColumnCorpus,
    sequence_tagger: str = "flair/ner-german-large",
    learning_rate: float = 1e-4,
    mini_batch_size: int = 64,
    max_epochs: int = 10,
    output_path: str = "./models/relation_extraction_1_epochs",
    transformer:str = "bert-base-german-cased"
):
    """Plot the loss learning rate curve."""

    # Step 2: Make the label dictionary from the corpus
    label_dictionary = corpus.make_label_dictionary("relation")
    label_dictionary.add_item("O")

    # Step 3: Initialize fine-tunable transformer embeddings
    embeddings = TransformerWordEmbeddings(model=transformer, layers="-1", fine_tune=True)

    # Step 4: Initialize relation classifier
    model: RelationExtractor = RelationExtractor(
        embeddings=embeddings,
        label_dictionary=label_dictionary,
        label_type="relation",
        entity_label_type="ner",
        # entity_pair_filters= [
        #    ('PER', 'PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, neuer_Name, verfolgt_von
        #    ('Coref_PER', 'PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, neuer_Name, verfolgt_von
        #    ('PER', 'Coref_PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, verfolgt_von
        #    ('Coref_PER', 'Coref_PER'), # Bekanntschaft, Ehe_mit, Gegener_von, Eltern/Kind, Arbeitskollegen, verfolgt_von
        #    ('PER', 'LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('Coref_PER', 'LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('PER', 'Coref_LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('Coref_PER', 'Coref_LOC'), # inhaftiert_in, arbeitet_in, wohnt_in, tätig_in
        #    ('PER', 'ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('Coref_PER', 'ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('PER', 'Coref_ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('Coref_PER', 'Coref_ORG'), # Mitglied_bei, arbeitet_bei, Gruppenmitglied, verfolgt_von, FÜhrungsposition_in
        #    ('ORG', 'LOC'), # liegt_in
        #    ('Coref_ORG', 'LOC'), # liegt_in
        #    ('ORG', 'Coref_LOC'), # liegt_in
        #    ('Coref_ORG', 'Coref_LOC'), # liegt_in,
        #    ('ORG', 'ORG'), # verfolgt_von
        #    ('ORG', 'Coref_ORG'), # verfolgt_von
        #    ('Coref_ORG', 'ORG'), # verfolgt_von
        #    ('Coref_ORG', 'Coref_ORG'), # verfolgt_von
        #   ]
    )

    # Step 5: Initialize trainer
    trainer: ModelTrainer = ModelTrainer(model, corpus)

    # 7. find learning rate
    learning_rate_tsv = trainer.find_learning_rate(
        'models/relation_extraction/learning_rate',
        Adam,
        mini_batch_size = 32,
        start_learning_rate = 1e-7,
        end_learning_rate = 10,
        iterations = 100,
        )
    # 8. plot the learning rate finder curve
    from flair.visual.training_curves import Plotter
    plotter = Plotter()
    plotter.plot_learning_rate(learning_rate_tsv)

def optim_params(
    corpus: ColumnCorpus,
):
    """Create search space for hyperparameter selection."""
    from flair.hyperparameter.param_selection import TextClassifierParamSelector, OptimizationValue

    from hyperopt import hp
    from flair.embeddings import FlairEmbeddings, StackedEmbeddings, WordEmbeddings
    from flair.hyperparameter.param_selection import SearchSpace, Parameter

    # define your search space
    search_space = SearchSpace()
    search_space.add(Parameter.EMBEDDINGS, hp.choice, options=[TransformerWordEmbeddings(model='bert-base-german-cased', layers="-1", fine_tune=True), TransformerWordEmbeddings(model='distilbert-base-uncased', layers="-1", fine_tune=True)])
    #search_space.add(Parameter.TRANSFORMER_MODEL, hp.choice, options=['bert-base-german-cased', 'distilbert-base-uncased'])
    #search_space.add(Parameter.TRANSFORMER_MODEL, hp.choice, options=['bert-base-german-cased', 'distilbert-base-uncased'])
    #search_space.add(Parameter.HIDDEN_SIZE, hp.choice, options=[32, 64, 128])
    #search_space.add(Parameter.RNN_LAYERS, hp.choice, options=[1, 2])
    #search_space.add(Parameter.DROPOUT, hp.uniform, low=0.0, high=0.5)
    search_space.add(Parameter.LEARNING_RATE, hp.choice, options=[1e-7, 1e-6, 1e-5, 1e-4, 1e-3])
    search_space.add(Parameter.MINI_BATCH_SIZE, hp.choice, options=[8, 16, 32])
        # what label do we want to predict?
    label_type = 'relation'

    from flair.hyperparameter.param_selection import SequenceTaggerParamSelector

    # create the parameter selector
    param_selector = SequenceTaggerParamSelector(corpus,
                                                'relation',
                                                'models/relation_extraction/params',
                                                training_runs=3,
                                                max_epochs=50
)

    # start the optimization
    param_selector.optimize(search_space, max_evals=100)


def _parse_args():
    parser = argparse.ArgumentParser(description="Train a relation extraction model on a corpus")
    parser.add_argument(
        "--data_path",
        type=str,
        help="Path to the data directory containing the data files in conll format, named: {train, val, test}.conll",
        required=True,
    )

    parser.add_argument(
        "--output_path",
        type=str,
        help="path to the output directory",
        # required=True,
        # default="models/ner",
    )

    parser.add_argument(
        "--max_epochs",
        type=int,
        help="max epochs",
        default=10,
    )

    parser.add_argument(
        "--batch_size",
        type=int,
        help="mini batch size",
        #default=32,
    )

    parser.add_argument(
        "--learning_rate",
        type=float,
        help="learning rate",
        default=1e-4,
    )

    return parser.parse_args()


if __name__ == "__main__":

    args = _parse_args()
    data_folder = args.data_path

    output_path = args.output_path
    max_epochs = args.max_epochs
    batch_size = args.batch_size
    learning_rate = args.learning_rate

    file_name_train = "train_no_coref.conll"
    file_name_dev = "val_no_coref.conll"
    file_name_test = "test_no_coref.conll"

    # create our corpus based on conll format data
    columns = {1: "text", 2: "ner"}

    # initializing the corpus
    corpus: Corpus = ColumnCorpus(
        data_folder,
        columns,
        train_file=file_name_train,
        dev_file=file_name_dev,
        test_file=file_name_test,
    )


    train(corpus=corpus,
    max_epochs=max_epochs,
    learning_rate=learning_rate,
    mini_batch_size=batch_size,
    output_path=output_path)

    # optim_params(corpus=corpus)

    # find_learning_rate(corpus=corpus,
    # max_epochs=max_epochs,
    # learning_rate=learning_rate,
    # mini_batch_size=batch_size,
    # output_path=output_path)