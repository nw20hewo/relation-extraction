# Relation Extraction

## Deadlines

- 20.06 Exposé
- 04.07 Project Presentation
- 29.08 Project Report

## Leitfragen

1. Was ist unser Ziel?
2. Mit welchen Daten arbeiten wir?
    - Sprache: Deutsch
    - Erweitern des Trainingsdatensatzes
    - Wikipedia Seiten crawlen (Widerstandskämpfer:innen)
    - Web Archive was steckt im WebArchive?
3. Wie müssen wir die Daten aufarbeiten?
4. Wie sieht unsere Architektur aus?
    - Verwendung von Flair
    - Welche Ansätze/Architekturen nutzen wir zum Vergleichen?
5. Wie evaluieren wir unseren Ansatz?
    - Händisch annotieren der Groundtruth von Wikipedia
    - Evaluation auf Wikipedia (Verwendung von Relations Heuristiken) vs unser Modell

## Todo
- [ ] Exposé schreiben
- [ ] Präsentation erstellen

## 09.06.

Planänderung - Lukas' Vorschlag 1

- Erstellung eines Goldstandard Datensatzes
- NER Recognition mit Flair
- Auf Basis davon Relationen händisch extrahieren
- Exposé auf Basis von Vorschlag 1

```
Fokus auf Daten-Aggregierung (Extraktion von Texten, die Relationen enthalten). Hier liegt der Hauptteil des Projektes darin, eine precision-orientierte Datenextraktions-Pipeline zu erstellen, um geeignete und qualitativ hochwertige Trainingsdaten aus dem Internet Archive zu distillieren. Dabei können auch Deep-Learning-Ansätze Anwendung finden. Der Fokus wäre hier weiterhin darauf, Texte zu finden, in denen Relationen enthalten sind, und nicht umbedingt, diese Relationen tatsächlich zu extrahieren. Auf die extrahierten Texte kann dann im zweiten Schritt eine vorhandene Baseline-Architektur (auch bestehende Implementationen) angewandt werden, um vergleichend zu testen, ob die Daten geeignet sind. 
```

## 20.06. Q&A Session

- Deadline für das Einreichen der Präsentation wahrscheinlich am Sonntag (03.07.)
- Inhalt der Präsentation: aktuellster Stand (mit Feedback aus 1:1 Sessions bestenfalls)
- Empfehlung: Festlegung interner Deliverables mit Rollen und Deadlines
- Einreichen eines Data Sheet/Model Card? -> Zweck spezifizieren
- Einreichen eiens Attachment Dokument mit Aufgabenverteilung am Ende z.B. in Tabellenform
- Personalpronomen in dem Exposé möglich ("we")
- Train-Test Leakage -> zeigen, dass wir es berücksichtig haben
- Größe des annotierten Datensatzes: Techniques z.B. Active Learning (?)

## 27.06. Einzelgespräch

- Ziel: hoher Recall oder doch Precision? wichtig für Motivation und Methodik -> entsprechende Anpassung nötig, zudem: Definition im Paper festhalten
- Pre-Trained Model von Flair: Quality Check! z.B. über händisches Überprüfen von 100 Entities -> haben wir eine korrekte Extraktion? F1-Score alleine wohl nicht ausreichend
- Subdomäne Freiheitskämpfer:innen nicht generalisierbar auf alle historischen Daten -> neuer Datensatz aus anderem Thema
- Bei GTP-3 Unterstützung menschliche Verifikation über eine "geschickte Auswahl" nötig, Achtung: Train-Test Leakage!
- generell jede Entscheidung auf Basis der Daten immer mit Gefahr des Train-Test Leakage -> weiterer Prozess nicht erneut auf Test-Daten sondern Validation-Daten
- Training sollte mehr in den Mittelpunkt gerückt werden
- Doccano-Instanz vom Webis (macht Nikki Deckers evtl. noch im Zug auf dem Heimweg): Finden einer ergonomischen Art des Annotieren (Vorgehen dokumentieren)
- bei Recall-basiertem Ansatz: Mitberücksichtigung versteckter Annotationen (Personalpronomen), sonst großer Informationsverlust zu beachten

## 11.07. Q&A Session

- Contribution Statement Inspiration https://www.elsevier.com/authors/policies-and-guidelines/credit-author-statement
- nicht zu viele technische Details aufgrund der Länge in Final Report (Seitenzahl noch unklar), Details eher in Readme
- Struktur des Papers: Introduction mit Motivation, Related Work, Data Set Description (+ Pipeline, Libraries), Experiments, Results, Conclusion (+ Discussion) -> bei uns aber wohl eher Abweichend aufgrund des Themas

## Links

- Google-Doc für Expose: https://docs.google.com/document/d/1GgaD72Jm79VriwI4WhvkeOvVTlli1u3Owb1q9sMSS-I/edit?usp=sharing
- Overleaf für Ausarbeitung: https://www.overleaf.com/2435769396wrpqrymtrfkc

### Google Colab
Links zu den unterschiedlichen Google Colab Notebooks:
- [Finetuning des Flair NER-Models](https://colab.research.google.com/drive/1JDJMaHLHDBXluiGpfFyTDuOU83wwcGtD?usp=sharing)
- [Training des Relation Extraction Modells](https://colab.research.google.com/drive/1kUiRhGZSSqbEowBtLQLQxpk5kpMuMF4F?usp=sharing)
- [Vorhersage von neuen Daten mit trainiertem NER und RE Modell](https://colab.research.google.com/drive/1aabVkOFaPvljQt4Q1G1fY1TAEZp012aL?usp=sharing)
