# The Connectionists

## Usage
To use the trained Relation Extraction Model install [flair](https://github.com/flairNLP/flair) 0.11 with torch 1.12.
With flair the trained models can easily be loaded and run on the chosen data.

Below you can find a code example assuming a similar file structure to load locally saved models.
This example is based on the [flair example for relation extraction](https://github.com/flairNLP/flair/releases/tag/v0.10.)

First install the requirements.txt into your environment.
````bash
pip install -r requirements.txt
````

````python
from flair.data import Sentence
from flair.models import RelationExtractor, SequenceTagger

# Uncomment this Workaround if you are on Windows to load RelationExtractors trained on UNIX-OS! (https://github.com/flairNLP/flair/issues/1052)
# import pathlib
# temp = pathlib.PosixPath
# pathlib.PosixPath = pathlib.WindowsPath

# 1. make example sentence
sentence = Sentence("Wie Elser im Berliner Verhörprotokoll erklärte, trat er 1928 oder 1929 in Konstanz dem Roten Frontkämpferbund bei, dem paramilitärischen Kampfverband der KPD.")

# 2. load entity tagger and predict entities
tagger = SequenceTagger.load('models/ner/ner-500/final-model.pt')
tagger.predict(sentence)

# check which entities have been found in the sentence
entities = sentence.get_labels('ner')
for entity in entities:
    print(entity)

# 3. load relation extractor
extractor: RelationExtractor = RelationExtractor.load('models/relation-extraction/re-500/final-model.pt')

# predict relations
extractor.predict(sentence)

# check which relations have been found
relations = sentence.get_labels('relation')
for relation in relations:
    print(relation)
````

## Dataset

The final dataset is located at final_dataset in [JSONL](https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction/-/blob/main/final_dataset/resistance.jsonl) and [ConLL](https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction/-/blob/main/final_dataset/resistance.conll) format.

It also contains the data [split](https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction/-/blob/main/final_dataset/split) we used to train the model.

## Project submissions

- The final paper for this project can be found [here](https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction/-/blob/main/paper/REsistance.pdf)
- The datasheet can be found [here](https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction/-/blob/main/Datasheet_for_dataset_template.pdf)
- The model card can be found [here](https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction/-/blob/main/Model_card_template.pdf)

