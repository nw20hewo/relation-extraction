import re
import scrapy


class ResistanceSpider(scrapy.Spider):
    name = "resistance"

    allowed_domains = ["wikipedia.org"]

    def start_requests(self):
        url = 'https://de.wikipedia.org/wiki/Liste_von_Widerstandsk%C3%A4mpfern_gegen_den_Nationalsozialismus'
        yield scrapy.Request(url=url, callback=self.parse_overview)

    custom_settings = {
            'DEPTH_LIMIT': 1
        }

    def parse_overview(self, response):
        content = response.css('div#mw-content-text')
        for li in content.xpath('.//ul//li'):
                url = li.xpath('.//a/@href').get()
                if url:
                    complete_url_next_page = response.urljoin(url)
                    yield response.follow(complete_url_next_page, callback=self.parse_subjects)
    
    def parse_subjects(self, response):
        title = response.css('h1#firstHeading::text').get()
        content = response.css('.mw-parser-output')
        content_list = content.xpath('.//p//text()').getall()
        text =  [re.sub(r"\[\d{1,3}\]", '', i) for i in content_list]
        yield {"name": title, "text": "".join(text)}
        

