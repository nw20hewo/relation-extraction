\section{Experiments}
\label{sec:experiments}
In the following section, we describe the experimental conditions of our method. The initial experiments and trials were conducted in the Google Colab \footnote{\url{https://colab.research.google.com/}} environment and then continued on the Webis Group cluster provided by the Digital Bauhaus Lab\footnote{\url{https://webis.de/facilities.html}} due to Google Colab's non-permanent availability of GPUs. 
% Because the GPU was accessible constantly, the final available models were then trained on the cluster. 
The training was performed using the data described in the previous section.
We split the data into three subsets: 80\% for training, 10\% for validation, and 10\% for testing. Training is performed on the training split. In the training process, the model parameters were adjusted using the validation set, and finally we used the test set to obtain an unbiased evaluation result.
The experiments were performed separately for the \ac{NER} and \ac{RE} tasks, each of which also considered coreferences and the absence of them.
By training our own model on the \ac{NER} task, we expected to simplify the subsequent \ac{RE} task, and thus to recognize more entities in our historical domain. Combining both models, our approach provides a holistic solution for recognizing Named Entities and relationships in individual sentences and texts.



\subsection{Training}

As already mentioned earlier, Flair is suitable for a variety of tasks within \ac{NLP} \cite{akbik-etal-2019-flair} and offers a promising starting point for our approach.
% The flair \ac{NER} model that we used to obtain Named Entity tags in the initial annotation process is trained as follows.

% \begin{itemize}
%     \item Training on CONLL\_03\_German Dataset
%     \item Using glove embeddings and contextual string embeddings forward and backward as a stacked embeddings
%     \item maximum epochs 150
% \end{itemize}

% We train our own \ac{NER} model to match the historical domain of the resistance fighters, and extending the base \ac{NER} model provided by flair with co-references.
% https://huggingface.co/flair/ner-german
To train the \ac{NER} model, the annotated data were first transferred from CoNLL format to a Flair ColumnCorpus.
From this corpus, we then extracted the \ac{NER} label dictionary for the training, i.e., the \ac{NER} tags as shown in Table \ref{table:NER_tags}.
Subsequently, we initialized the \texttt{Bert-base-german-cased} model, fine-tunable transformer embeddings with document context from HuggingFace\footnote{\url{https://huggingface.co/bert-base-german-cased}}.
Based on these embeddings and our label dictionary, we created a \ac{NER} tagger via the Flair \texttt{SequenceTagger} class for the training process.
This sequence tagger was fine-tuned with the \texttt{ModelTrainer} class of Flair, with a learning rate of \(5.0e^{-6}\), a batch size of 32, and Adam used as the optimizer \cite{kingma2014adam}.

To determine the model parameters, we used the Model Tuning included in Flair.
For this purpose, we defined the parameter search space in advance to compare different transformer embeddings and batch sizes.
The choice of learning rate is based on the Flair implementation of \textit{Smith's method} for determining the learning rate \cite{smith-2015-learning-rate}. In doing so, we considered the Figure \ref{fig:learning_rate} and determined the point at which the loss decreases most steeply.

 \begin{figure}[htbp]
        \centering
        \includegraphics[width=\linewidth]{Images/learning_rate.png}
        \caption{Loss with respect to the learning rate for the fine-tuning of the \ac{RE} model based on the transformer embeddings. The optimal range for the learning rate is where the loss decreases the most.}
        \label{fig:learning_rate}
    \end{figure}

%\begin{itemize}
%    \item Dataset statistics
%    \item Model - experimental settings (using Flair, CoNLL Data Format)
%    \item Evaluation (dependent on our goal: recall vs. precision vs. F1?): verification of generalization using external data, compare our approach against a baseline, include plots or other visualizations
%\end{itemize}

Given a sentence with Named Entities as input, our models aims to determine a relation between entity pairs.
A common method to split the entire dataset for a model is to shuffle all the data instances and then to assign them to one of the sets. However, this step involves the risk of receive a leakage, where data points are considered in the training process which are not intended to be used in this phase and therefore potentially lead to a biased performance evaluation \cite{kaufman2012leakage}. This can be the case in both \ac{RE} and \ac{NER} tasks when training and test data resemble each other \cite{elangovan-etal-2021-memorization}. Given the source of our data, we can assume that sentences of the same Wikipedia entry may be very similar to each other. Consequently, to avoid a train-test leakage, we decided to first group our data by entry, hence by person, and to split the dataset afterwards.

To train the \ac{RE} model, we transferred the annotated sentences to a Flair corpus in the form of a ConLL dataset, as was already done for the training of the \ac{NER} model. From this corpus, the existing different relation classes were transferred to a label dictionary.
Subsequently, pre-trained transformer embeddings loaded for fine-tuning, in our case \texttt{Bert-base-german-cased}.
We then used Flair's \texttt{RelationExtractor} class to fine-tune the model based on the embeddings, the label dictionaries and our corpus.
Here we fine-tuned the model for 100 epochs with a batch size of 64, the learning rate was set to \(1.0e^{-5}\), and Adam was used as the optimizer. 
The models are evaluated based on the micro F1 score.


\subsection{Evaluation of Baseline}\label{subsec:baseline}

A heuristic based approach developed by one of our team members served us as a baseline method \cite{Ric:22}. Its \ac{RE} process considered primarily two lexical features, \ac{POS} tags, on the one hand and trigger words on the other. More precisely, \ac{POS} tags were used to select only such words from the provided sentences that have the highest information content. These were derived by extracting all finite full verbs, all uninflected full verbs and all detached verb affixes and while ignoring auxiliaries verbs to finally create the relations based on this selection. This step was then complemented by the use of the aforementioned trigger words to pursue the intention of taking also Casus and Numerus into account. The found relations were then grouped together based on the involved entity types to ensure meaningful relations and generalization of similar relations simultaneously.

To compare the results of the trained \ac{RE} model with this baseline, the test set we created was run through this described pipeline.

It has to be noted, however, that not only the baseline approaches of \ac{NER} and \ac{RE} differ significantly from the approach described here.
But also, the resulting output format of the base model varies widely from the format of the annotated dataset.
Because of these differences, we decided that it was sufficient to evaluate the baseline method's ability to detect the presence of relations in a given text.
We considered its best-case scenario and assumed that each found relation is correct regarding involved entities and relation classes. The output relations classified by our baseline were then compared to the actual annotations of the test set in terms of presence of a relation, which showed that this method had a Recall of 17.4 \% and a Precision of 93.5 \% with a F1 score of 0.294.
\begin{table}[htbp]
\footnotesize
    \centering
    \begin{tabularx}{\linewidth}{l|XX}
         & \makecell{Predicted Positive} & \makecell{Predicted Negative} \\
         \hline
        \makecell{Positive} & \makecell{72} & \makecell{341} \\
        \makecell{Negative} & \makecell{5} & \makecell{67} \\
    \end{tabularx}
    \caption{Confusion Matrix of baseline method.}
    \label{table:confusion_matrix}
\end{table}


\subsection{But does it generalize?}
To prove that the introduced \ac{RE} model is indeed able to generalize knowledge from this very specific domain, it was run over Wikipedia entries of a random subset of a list of biographies in German language\footnote{\url{https://de.wikipedia.org/wiki/Liste_der_Biografien}}(WikiBioData).
