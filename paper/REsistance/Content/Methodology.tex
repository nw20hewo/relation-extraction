\section{Methodology}

\label{sec:methodology}
In the following section, we present our process for creating the dataset. Our workflow can be seen in Figure \ref{fig:workflow}. We provide an overview of how we obtained, preprocessed, and annotated the data using Active Learning.
\begin{figure}[htbp]
        \centering
        \includegraphics[width=\linewidth]{Images/Workflow.png}
        \caption{The conceptualization of chronological workflow to first create a dataset and second build a model to apply to that dataset.}
        \label{fig:workflow}
    \end{figure}
\subsection{Data Scraping and Preprocessing}
As mentioned earlier, we want to address use cases for \ac{RE} in the historical domain, more precisely those concerning the National Socialist period in Germany. To obtain a database on which an iterative annotation process could take place, we scraped data about resistance fighters to National Socialism. In this regard, by using Scrapy\footnote{\url{https://github.com/scrapy/scrapy}} we obtained biographical information in German from a list of 945 Wikipedia entries\footnote{\url{https://de.wikipedia.org/wiki/Liste_von_Widerstandskämpfern_gegen_den_Nationalsozialismus}} of which 64 were actually annotated later.
Subsequently, the scraped Wikipedia data was split into individual sentences for each person. 
% Afterwards we performed named entity r

\subsection{Annotation}

For an ergonomic annotation process, the Doccano software \cite{doccano} was chosen, to annotate both Named Entity and relation instances. It is considered a lightweight and easy to use annotation tool, for which we deployed an instance on Heroku\footnote{\url{https://www.heroku.com/}}. During the actual annotation process, due to time constraints it was not possible to have a second person to review each annotation, but at least those relations where doubts arose, were checked by multiple people. To ensure a consistent annotation among us, a guideline was written in Doccano and furthermore the annotation process was visually supported by color groupings.

\begin{table}[htbp]
    \footnotesize
    \centering
    \begin{tabularx}{\linewidth}{l|l}

         Tag & Meaning \\
    \hline
         PER & Person name \\
    
         LOC & Location name \\
    
         ORG & Organization name \\
    
         MISC & Miscellaneous name \\
         
         Coref\_PER & Coreference to a PER \\
    
         Coref\_LOC & Coreference to a LOC \\
    
         Coref\_ORG & Coreference to a ORG \\
    
         Coref\_MISC & Coreference to a MISC \\

    \end{tabularx}
    \caption{Our custom tag set based on Flair's standard 4-class \ac{NER} model for German extended by coreference tags. All those Named Entities fall into the "miscellaneous" category, that do not unambiguously belong to any of the other listed categories \cite{benikova-etal-2014-nosta, tjong-kim-sang-de-meulder-2003-introduction}.
}
    \label{table:NER_tags}
\end{table}

\paragraph{Named Entity and Coreference Annotation}\mbox{}\\
%Before annotating the relations, first it is necessary to ensure that all the entities in the examined sentences are marked as such, since relations can only be found between entity pairs. For this process, existing frameworks can be used to facilitate the manual annotation. 
In \ac{IE}, the research project FashionBrain has been able to deliver outstanding result. In this context the team members have focused, among other topics, on \ac{NER} and on the main task of our contribution, the \ac{RE} \cite{checco2017fashionbrain}, whereby the framework \textit{Flair} was developed for both purposes. Thus, Flair offers a state-of-the-art model for \ac{NER} \cite{akbik-etal-2018-contextual} that is able to automatically recognize entities within a sentence and assign them to a type. When applied to our sentences, while mainly persons and locations were classified quite accurately, the difficulty was that some organizations (e.g., 'Résistance', 'Volksgerichtshof') or other entities with a specific historical reference were not recognized. In order to obtain the relations that are associated to the unrecognized entities, we first had to annotate them on ourselves.
Additionally, inspired by other promising approaches in \ac{RE} that involve the development of a dataset and also a training process of a model, where the authors indicate that they achieve higher Recall and F1 scores when including coreferences, we extended our annotation process by covering them as well \cite{chan-roth-2010-exploiting,gabbard-etal-2011-coreference,luan-etal-2018-multi}. A coreference can be declared as the property of different nominal expressions or pronouns to refer to the same reference object or reference identity \cite{crystal2011dictionary}. For this purpose, we introduced additional tags that contain the information that the annotated word is not a single entity but in fact a coreference and also the type of the entity to which these coreferences refer (e.g, Coref\_LOC). This represented another manual effort for us, since persons represented by personal pronouns cannot be recognized directly and require an accurate procedure. 
\paragraph{Relation Annotation}\mbox{}\\
Simultaneously with the annotation of the entities came the annotation process of the relations. In the initial phase of determining the number of different annotation types took place, we were faced the trade-off between information preservation and generalization. Hereby we tried to find a balance and consequently settled for 46 different relation types, especially between the entity pairs \textit{PER-PER, PER-ORG, PER-LOC}. Looking at the semantic structure of the chosen classes, we can roughly identify a hierarchy in terms of specificity. Within this order we tried to assign the most fine-grained one to a given relation and at while using a more coarse class if none of the specific classes could be considered suitable. Initially, we only annotated a small subset of the entire data from those 945 Wikipedia entries before performing Active Learning. 

%    Try to find middle way between generalization and information preservation
%    Focus especially on diverse relations between the entity-pairs \textit{PER-PER, PER-ORG, PER-LOC}.
%    Hence the decision to work with 45 different relation classes.
%    At the time these 45 relation classes were decided, plan was to annotate the whole dataset of 945 Wikipedia entries.
    
%    In the annotation process the aim was to always use the most specific relation class possible.
%    The more general classes were only used when no specific one fitted.
    
%    To retrieve as many relations as possible also additional Named Entities were annotated.
%    On the one hand there were many organisations and other entities specific to the special historic domain, that were not recognised by the original Flair \ac{NER}-model (e.g. 'Résistance', 'Volksgerichtshof')
%    On the other hand in many sentences especially persons were hidden by coreferences like personal pronouns. Therefore these were also annotated with special tags indicating which kind of entity they are referencing. 
    
 \begin{figure}[htbp]
        \centering
        \includegraphics[width=\linewidth]{Images/Annotation_Example.png}
        \caption{Example of an annotation. The underline marks the length of an entity, below is the entity type, and the arrow marks an undirected relation containing the corresponding relation type. Above the sentence to be annotated, we see the editing options Doccano offers.}
        \label{fig:annotation}
    \end{figure}

%Afterwards we converted our tagged data from Doccano's output file format JSONL to the BIO format which was originally introduced by \citet{ramshaw-marcus-1995-text} and is used by Flair as an annotation format for spans. This is a common tagging format where entity tags are extended with prefixes to indicate words as the beginning or end of an entity when it consists of multiple words, for example \cite{tjong-kim-sang-de-meulder-2003-introduction}.

    
\subsection{Active Learning}
To compensate for the limited resources in terms of time and cost for the annotation process \cite{Akbik2016ExploratoryRE}, we used an Active Learning approach. This is an iterative method that aims to obtain a high predictive performance despite an insufficient amount of labeled data \cite{settles2009active} and has already proven effective for \ac{RE} tasks \cite{angeli-etal-2014-combining}. We implemented Active Learning by successively training the \ac{RE} model with the newly annotated data. This allows us to correct improper annotation and add missing ones. 

However, we have not yet automated this process to the point where changes to the annotations in Doccano directly retrain the model. Although Doccano itself supports auto-labeling/Active Learning, we had technical issues deploying our model to one of the clouds provided by Doccano (e.g., AWS, Google Cloud). Optimally, the sentences to be annotated would be sent to the deployed model via REST request, and an auto-labeled sentence would be returned directly to the Doccano interface.
Instead, we exported the corrected and annotated sentences from Doccano and then used them as new training data.
With this data, we retrained the \ac{NER} and \ac{RE} models.
We then applied the resulting model to predict annotations on new data and to subsequently check or readjust them in the Doccano environment.

% \subsection{NER Model}
% To recognize domain specific entities, and extend the recognized entities by coreferences, we trained a \ac{NER} model using Flair.
% The model leverages transformer-based embeddings. 

\subsection{Data Statistics}
The dataset resulting from the annotating and Active Learning process called \textit{REsistance} consists of 2980 sentences containing, 4312 annotated relations of 46 different relation classes as depicted in Table \ref{tab:overview_re} in the appendix
with an average number of tokens per sentence of 20.59.

\subsection{Dataset format}

The REsistance dataset is available as JSONL as well as ConLL file. A single sentence is stored with its text, the position of the entities and their types as well as the position of the relations, which are composed of the entity positions, and the corresponding relation type.
An example of a sentence in the ConLL format can be seen in listing \ref{listing:conll} in the appendix.
