\section{Results}
\label{sec:results}
% In the following we will present the results of our model.
% After only two iterations of our Active Learning method the resulting model already outperforms the baseline heuristic approach.

In the first part of this section, we will go through the results of our \ac{NER} model, trained on our domain-specific data, compared to the baseline: the \texttt{Flair NER German Large} model \cite{schweter2020flert}.
In the second part, we will show the results obtained by our RE model.
We have trained both models as described in the experiments section where we split the data into three sets: 80\% train, 10\% validation and 10\% test.
To address the train-test leakage problem, individuals are exclusively part of one split.

Table \ref{tab:f1_ner} depicts the results of our trained \ac{NER} model compared to the baseline Flair \ac{NER} model once on the test split of our domain data.

\begin{table}
\footnotesize
\centering
\begin{tabularx}{\linewidth}{l|XX}

%                & \multicolumn{2}{c}{Test} & \multicolumn{2}{c}{Bio} \\

                  & Our \ac{NER} model & Flair's \ac{NER} model\\% &  \makecell{Our model}  & \makecell{Flair \ac{NER}}  &   \\
\hline
PER               & 0.944          & \textbf{0.958}          \\%& 0.956          & \textbf{0.972} &   \\
ORG               & \textbf{0.734} & 0.700          \\%& 0.677          & \textbf{0.807} &   \\
LOC               & \textbf{0.936} & 0.935          \\%& 0.884          & \textbf{0.933} &   \\
MISC              & 0.706          & \textbf{0.813} \\%& 0.632          & \textbf{0.760} &   \\
Coref\_PER        & \textbf{0.728} & 0.000          \\%& \textbf{0.778} & 0.000          &   \\
Coref\_ORG        & \textbf{0.286} & 0.000          \\%& \textbf{1.000} & 0.000          &   \\
Coref\_LOC        & \textbf{0.333} & 0.000          \\%& 0.000          & 0.000          &   \\
\hline
MICRO F1          & \textbf{0.852} & 0.803          \\%& 0.798          & \textbf{0.813} &   \\
%MICRO F1 OF MODEL
\end{tabularx}
\caption{Micro F1 scores of the \ac{NER} model compared to the Flair's \ac{NER} model on our dataset listed for each tag. The higher value for an entity type is printed in bold.}
\label{tab:f1_ner}
\end{table}
The results show that our model and the Flair \ac{NER} base model perform very similarly on most Named Entity types on the domain-specific test dataset, so that the results per type differ only marginally. The exception are coreference entity types we introduced, which the base model can not recognize, but which are crucial for our use case, hence we also consider them in the overall evaluation, resulting in our new model having a higher F1 score across all entity types.


%\begin{table}
%\centering
%\scriptsize
%\begin{tabularx}{\linewidth}{l|ccccc}

%                & \multicolumn{2}{c}{Test} & \multicolumn{2}{c}{Bio} \\

%                  & \makecell{Our Model} & \makecell{Flair \ac{NER}} &  \makecell{Our model}  & \makecell{Flair \ac{NER}}  &   \\
%\hline
%PER               & 0.944          & 0.958          & 0.956          & \textbf{0.972} &   \\
%ORG               & \textbf{0.734} & 0.700          & 0.677          & \textbf{0.807} &   \\
%LOC               & \textbf{0.936} & 0.935          & 0.884          & \textbf{0.933} &   \\
%MISC              & 0.706          & \textbf{0.813} & 0.632          & \textbf{0.760} &   \\
%Coref\_PER        & \textbf{0.728} & 0.000          & \textbf{0.778} & 0.000          &   \\
%Coref\_ORG        & \textbf{0.286} & 0.000          & \textbf{1.000} & 0.000          &   \\
%Coref\_LOC        & \textbf{0.333} & 0.000          & 0.000          & 0.000          &   \\
%\hline
%MICRO F1          & \textbf{0.852} & 0.803          & 0.798          & \textbf{0.813} &   \\
%MICRO F1 OF MODEL
%\end{tabularx}
%\caption{Micro F1 scores of the \ac{NER} model on our dataset and on the comparison dataset listed for each tag.}

%\end{table}
% It can be noted that the coreference tags that are more represented in the dataset, i.e., \textit{Coref\_PER}, result in higher evaluation scores (e.g., precision, recall, F1-score).

% From this, we conclude that more coreference tags in our training dataset for \textit{Coref\_LOC} and \textit{Coref\_ORG} would significantly increase the evaluation scores in these coreference tags.

\begin{table}[htbp]
\scriptsize
    \centering
    \begin{tabularx}{\linewidth}{X|XX}
         & \makecell{Model with Coref} & \makecell{Model no Coref} \\
         \hline
         REsistance dataset & \makecell{0.466} & \makecell{0.333}\\
         WikiBioData & \makecell{0.283} & \makecell{0.336}\\
    \end{tabularx}
    \caption{Micro F1 scores of the \ac{RE} model on our dataset and on the comparison dataset considering the presence and absence of coreferences.}
    \label{tab:f1_relation_extraction}
\end{table}

Table \ref{tab:f1_relation_extraction} shows the results of the \ac{RE} model on the previously unseen test split of \textit{REsistance} and the generalizability comparison dataset. We trained the RE model once with the \textit{REsistance} dataset containing coreferences and once without to verify if this had an impact on the performance of the RE model. This seems to be the case to a limited extent, as the model performs slightly better on the generalizability dataset without coreferences.

To understand this F1 score in terms of the domain-specific \ac{RE} task, we compare our model with other domain-specific \ac{RE} models. The \ac{RE} approach of \citet{DBLP:journals/corr/abs-2004-03283} was applied to a German corpus of traffic and industrial events and achieve a maximum F1 score of 0.28, while the domain-specific approaches on English corpora reach maximum F1 scores of 0.474 and 0.6 \cite{10.1145/3038912.3052708, bioMedReEx}.
Indicating that our model is in the upper range of models for relation extraction in domain-specific applications, more specific in the German language.

As presented in subsection \ref{subsec:baseline} the F1 score of the baseline method is 0.294 which our trained model is able to exceed with a F1 score of 0.466.
Interpreting the results of the heuristic method as optimistically as possible and our \ac{RE} model as pessimistically as possible with the assumption of the worst possible outcome, we can still assume that our model is better able to recognize existing relations in sentences.

Table \ref{tab:overview_re} in the Appendix \ref{sec:appendix} shows the distribution of the individual relations in the dataset and the respective F1 scores evaluated by our final \ac{RE} model on our dataset and the comparison dataset. On the one hand, this result suggests that relations that occur more frequently in the dataset are also recognized correctly more frequently; on the other hand, this is not true for all relations. This rather leads to the assumption that certain relations are more difficult to detect than others. Indicating that increasing the number of data points for specific poor performing relation types in the dataset would not provide to much improvement. For instance, there are relations, such as \textit{Ausbildung\_an}, that achieve an F1 score of 0.8 even with a mediocre occurrence of 46 samples in the data set, while \textit{Gruppenmitglied}, \textit{Bekanntschaft}, and \textit{Führungsposition} score much worse with significantly more frequent occurrences. This observation is in line with the results of the confusion matrix in Figure \ref{fig:cm}, which already suggests that certain relations cannot be distinguished from each other with sufficient Precision. Furthermore, the evaluation on the generalization dataset shows that particular relations are generalizable, while others are either not represented in the new dataset or perform relatively poorly. During the annotation process of the generalization dataset, we also noticed that specific relations such as \textit{kollaboriert\_mit} have different semantic meanings depending on the context, which can also complicate the generalization of relations.



\begin{table}[htbp]
\scriptsize
    \centering
    \begin{tabularx}{\linewidth}{l|XXX}
         & \makecell{arbeitet\_in} & \makecell{wohnt\_in} & \makecell{Aufenthalt\_in}  \\
         \hline
         arbeitet\_in & \makecell{18} & \makecell{0} & \makecell{0} \\
         wohnt\_in & \makecell{5} & \makecell{10} & \makecell{0} \\
         Aufenthalt\_in & \makecell{1} & \makecell{0} & \makecell{12}\\
    \end{tabularx}
    \caption{Excerpt from the Confusion Matrix, indicating that these three relation classes are very similar. It can be assumed that the model will achieve better results if they are merged into one class.}
    \label{tab:conf_matrix_relation_classes}
\end{table}


Table \ref{tab:conf_matrix_relation_classes} shows an excerpt from the confusion matrix that highlights a source of error besides not identifying relations, namely classification confusion with other relation classes. We can address this by merging certain relations, such as relations in this case, that connect the PER entity with the LOC entity and furthermore have a high semantic similarity.