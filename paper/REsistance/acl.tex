% This must be in the first 5 lines to tell arXiv to use pdfLaTeX, which is strongly recommended.
\pdfoutput=1
% In particular, the hyperref package requires pdfLaTeX in order to break URLs across lines.

\documentclass[11pt]{article}

% Remove the "review" option to generate the final version.
\usepackage[]{acl}

% Standard package includes
\usepackage{times}
\usepackage{latexsym}

% For proper rendering and hyphenation of words containing Latin characters (including in bib files)
\usepackage[T1]{fontenc}
% For Vietnamese characters
% \usepackage[T5]{fontenc}
% See https://www.latex-project.org/help/documentation/encguide.pdf for other character sets

% This assumes your files are encoded as UTF8
\usepackage[utf8]{inputenc}

% This is not strictly necessary, and may be commented out,
% but it will improve the layout of the manuscript,
% and will typically save some space.
\usepackage{microtype}
\usepackage[nolist, nohyperlinks]{acronym}

% If the title and author information does not fit in the area allocated, uncomment the following
%
%\setlength\titlebox{<dim>}
%
% and set <dim> to something 5cm or larger.

\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{makecell}
\usepackage{float}
\usepackage{hyperref}
\usepackage{listings}
\lstset{literate=%
  {Ö}{{\"O}}1
  {Ä}{{\"A}}1
  {Ü}{{\"U}}1
  {ß}{{\ss}}1
  {ü}{{\"u}}1
  {ä}{{\"a}}1
  {ö}{{\"o}}1
}

\pagenumbering{arabic}

\title{REsistance: A German Dataset for Relation Extraction}

% Author information can be set in various styles:
% For several authors from the same institution:
% \author{Author 1 \and ... \and Author n \\
%         Address line \\ ... \\ Address line}
% if the names do not fit well on one line use
%         Author 1 \\ {\bf Author 2} \\ ... \\ {\bf Author n} \\
% For authors from different institutions:
% \author{Author 1 \\ Address line \\  ... \\ Address line
%         \And  ... \And
%         Author n \\ Address line \\ ... \\ Address line}
% To start a seperate ``row'' of authors use \AND, as in
% \author{Author 1 \\ Address line \\  ... \\ Address line
%         \AND
%         Author 2 \\ Address line \\ ... \\ Address line \And
%         Author 3 \\ Address line \\ ... \\ Address line}

\author{Jonas Richter, Nils Wenzlitschke, Viet Dung Le\\
  Faculty of Mathematics and Computer Science \\
  University Leipzig \\
  04109 Leipzig \\
  \texttt{\{jr74xaqo, nw20hewo, vl38vesi\}@studserv.uni-leipzig.de} \\}

\begin{document}
\maketitle

\begin{acronym}
\acro{NER}{Named Entity Recognition}
\acro{IE}{Information Extraction}
\acro{RE}{Relation Extraction}
\acro{NLP}{Natural Language Processing}
\acro{POS}{Part of Speech}
\end{acronym}


\begin{abstract}
Detecting and classifying semantic relationships into categories between previously given entities from a text has become a significant task in Natural Language Processing and is referred to as Relation Extraction. Just like in other areas that aim to extract information from unstructured texts, extensive datasets already support the development and evaluation of approaches in English, whereas there are comparatively few in other languages. Accordingly, in German there is a lack of suitable datasets and consequently of models, which we would like to address with this work. This paper describes the combination of manual annotation and an Active Learning approach in order to create the Relation Extraction dataset called \textit{REsistance} based on biographies of resistance fighters against National Socialism. This dataset consists of 2980 sentences with 4312 annotated relations of 46 different relation classes. In addition, we introduce a model for Relation Extraction using Flair on this dataset. Using a transformer-based method, it provides results that indicate it performs better than a rule-based heuristic approach and, furthermore, that it can be generalized to other biographical text data to a certain degree.

\end{abstract}


\input{Content/Introduction}

\input{Content/Related Work}

\input{Content/Methodology}

\input{Content/Experiments}

\input{Content/Results}

\input{Content/Conclusion}

\section*{Author Contributions}
\textbf{Jonas Richter}: Conceptualization, Data curation, Methodology, Software, Writing - Reviewing. \textbf{Nils Wenzlitschke}: Conceptualization, Methodology, Data curation, Software, Writing- Reviewing and Editing. \textbf{Viet Dung Le}: Investigation, Visualization, Data curation, Writing- Original draft preparation.

% Entries for the entire Anthology, followed by custom entries
\bibliography{anthology,custom}
\bibliographystyle{acl_natbib}
\newpage
\appendix

\section{Appendices}
\label{sec:appendix}
Our results are publicly accessible on \href{https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction}{https://git.informatik.uni-leipzig.de/nw20hewo/relation-extraction}
%Appendices are material that can be read, and include lemmas, formulas, proofs, and tables that are not critical to the reading and understanding of the paper. 
%Appendices should be \textbf{uploaded as supplementary material} when submitting the paper for review.
%Upon acceptance, the appendices come after the references, as shown here.

%\paragraph{\LaTeX-specific details:}
%Use {\small\verb|\appendix|} before any appendix section to switch the section numbering over to letters.


% \section{Supplemental Material}
% \label{sec:supplemental}
% Submissions may include non-readable supplementary material used in the work and described in the
% paper. Any accompanying software and/or data
% should include licenses and documentation of research review as appropriate. Supplementary material may report preprocessing decisions, model
% parameters, and other details necessary for the replication of the experiments reported in the paper.
% Seemingly small preprocessing decisions can sometimes make a large difference in performance, so
% it is crucial to record such decisions to precisely
% characterize state-of-the-art methods.
% Nonetheless, supplementary material should be
% supplementary (rather than central) to the paper. \textbf{Submissions that misuse the supplementary material may be rejected without review.}
% Supplementary material may include explanations

\begin{table}[H]
\centering
\scriptsize
\begin{tabular}{l|ccc}
Relation             & Samples & REsistance & WikiBioData  \\
\hline
Mitglied\_bei        & 392     & 0.597     & 0.000        \\
Aufenthalt\_in       & 322     & 0.535     & 0.500        \\
arbeitet\_in         & 262     & 0.507     & 0.000        \\
Bekanntschaft        & 234     & 0.375     & 0.000        \\
Gruppenmitglied      & 224     & 0.438     & 0.800        \\
liegt\_in            & 217     & 0.529     & 0.462        \\
Führungsposition\_in & 199     & 0.286     & 0.000        \\
wohnt\_in            & 178     & 0.359     & 0.667        \\
arbeitet\_bei        & 160     & 0.364     & 0.700        \\
Gegner\_von          & 159     & 0.444     & 0.000        \\
inhaftiert\_in       & 146     & 0.462     & N/A          \\
tätig\_in            & 143     & 0.364     & N/A          \\
Kontakt\_zu          & 122     & 0.222     & 0.000        \\
neuer\_Name          & 107     & 0.381     & 0.000        \\
Arbeitskollegen      & 106     & 0.174     & 0.200        \\
Kurzform             & 104     & 0.286     & 0.000        \\
Eltern/Kind          & 99      & 0.870     & 1.000        \\
Ehe\_mit             & 97      & 0.483     & N/A          \\
Geschwister          & 97      & 0.000     & N/A          \\
verfolgt\_von        & 93      & 0.737     & N/A          \\
geboren\_in          & 83      & 0.800     & 1.000        \\
Standort             & 81      & 0.462     & N/A          \\
gestorben\_in        & 80      & 0.933     & N/A          \\
Ausbildung\_in       & 68      & 0.800     & 0.000        \\
Flucht\_nach         & 58      & 0.833     & N/A          \\
kollaboriert\_mit    & 57      & 0.000     & 0.000        \\
Ausbildung\_an       & 45      & 0.800     & 0.000        \\
Opposition\_zu       & 41      & 0.000     & N/A          \\
stationiert\_in      & 38      & 0.000     & N/A          \\
Unterstützung\_von   & 38      & 0.000     & N/A          \\
Rückkehr\_nach       & 35      & 0.222     & N/A          \\
Teilgruppe\_von      & 33      & 0.000     & N/A          \\
liegt\_bei           & 30      & 0.000     & N/A          \\
ermordet\_in         & 25      & 0.571     & N/A          \\
Freundschaft         & 25      & 0.000     & N/A          \\
besetzt\_von         & 23      & 0.000     & N/A          \\
ausgeschlossen\_von  & 17      & 0.000     & N/A          \\
ausgetreten\_bei     & 12      & 0.000     & 0.000        \\
liegt\_an            & 12      & N/A       & N/A          \\
Flucht\_aus          & 11      & 0.000     & N/A          \\
Verwandt\_mit        & 11      & 0.000     & N/A          \\
eingezogen\_zu       & 10      & 0.000     & N/A          \\
Beziehung\_mit       & 7       & 0.000     & N/A          \\
Außenlager\_von      & 6       & 0.000     & N/A          \\
Trennung\_von        & 3       & 0.000     & N/A          \\
befreit\_von         & 2       & N/A       & N/A          \\
\end{tabular}
\caption{Distribution of relations in the overall dataset with their respective F1 scores of recognition by our final Relation Extraction Model on our dataset and the generalizability dataset. N/A stands for not available in evaluated dataset.}
\label{tab:overview_re}
\end{table}

\newpage

\begin{table}[htbp]
\centering
\begin{tabular}{l|c}
Entity     & Samples  \\
\hline
PER        & 2242   \\
LOC        & 1880   \\
ORG        & 1349   \\
MISC       & 247    \\
Coref\_PER & 1068   \\
Coref\_LOC & 36     \\
Coref\_ORG & 35    
\end{tabular}
\caption{Distribution of named entities in the dataset with respective F1 scores for NER recognition. Comparing the trained NER model on our dataset with the generalizability dataset.}
\label{tab:overview_ner}
\end{table}

\newpage

\begin{lstlisting}[basicstyle=\small,label={listing:conll}, breaklines,caption={Example sentence from the \textit{REsistence} dataset in the ConLL format.},captionpos=b]
# text = Ab 1909 organisierte er sich im Deutschen Metallarbeiterverband ( DMV ) , wurde von 1910 bis 1917 Mitglied der SPD , anschließend trat er der USPD bei und später der KPD .
# relations = 7;8;10;10;Kurzform|4;4;7;8;Mitglied_bei|4;4;20;20;Mitglied_bei|4;4;26;26;Mitglied_bei|4;4;31;31;Mitglied_bei|4;4;20;20;ausgetreten_bei
1	 Ab	 O
2	 1909	 O
3	 organisierte	 O
4	 er	 B-Coref_PER
5	 sich	 O
6	 im	 O
7	 Deutschen	 B-ORG
8	 Metallarbeiterverband	 I-ORG
9	 (	 O
10	 DMV	 B-ORG
11	 )	 O
12	 ,	 O
13	 wurde	 O
14	 von	 O
15	 1910	 O
16	 bis	 O
17	 1917	 O
18	 Mitglied	 O
19	 der	 O
20	 SPD	 B-ORG
21	 ,	 O
22	 anschließend	 O
23	 trat	 O
24	 er	 B-Coref_PER
25	 der	 O
26	 USPD	 B-ORG
27	 bei	 O
28	 und	 O
29	 später	 O
30	 der	 O
31	 KPD	 B-ORG
32	 .	 O
\end{lstlisting}



\begin{figure*}[htbp]
        \centering
        \includegraphics[width=\linewidth]{Images/confusion_matrix_final.png}
        \caption{Confusion matrix of the predictions of the Relation Extraction model and the corresponding gold labels. Horizontally shown are the gold labels, vertically the predictions.}
        \label{fig:cm}
\end{figure*}

\newpage

Icons of Figure \ref{fig:workflow} made by Becris, Darius Dan, Freepik, kmg design, Parzival' 1999 and Pixel perfect from flaticon.com


\end{document}

